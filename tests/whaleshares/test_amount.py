from whaleshares.amount import Amount


def test_amount_init():
    a = Amount('1 WLS')
    assert dict(a) == {'amount': 1.0, 'asset': 'WLS'}
